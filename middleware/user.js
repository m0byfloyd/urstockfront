export default function ({ store, redirect, route }) {
  const guestRoutes = [
    '/homepage',
    '/login',
    '/connection',
    '/register',
    '/register/address',
    '/register/company'
  ]
  if (!store.state.auth.loggedIn && !guestRoutes.includes(route.path.toLowerCase())) {
    return redirect('/homepage')
  }
}
