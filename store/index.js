export const state = () => ({
  registerData: {
    account: {},
    address: {},
    company: {}
  }
})

export const getters = {
  getRegisterData (state) {
    return state.registerData
  }
}

export const mutations = {
  setRegisterData (state, payload) {
    switch (payload.dataStep) {
      case 'account':
        state.registerData.account = payload.account
        break
      case 'address':
        state.registerData.address = payload.address
        break
      case 'company':
        state.registerData.company = payload.company
        break
      default:
        break
    }
  }
}
