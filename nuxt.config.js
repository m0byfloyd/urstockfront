export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'urstock-front',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/logo-urstock.png' }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['assets/global.css', 'assets/Fonts.css', 'assets/Variables.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/auth-next'
  ],

  router: {
    middleware: ['user', 'disconnected'],
    extendRoutes (routes, resolve) {
      routes.push(
        {
          name: 'LoginPage',
          path: '/login',
          component: resolve(__dirname, 'pages/Connection.vue')
        }
      )
    }
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/authentication_token',
            method: 'post',
            propertyName: 'token'
          },
          user: true,
          logout: true
        }
      }
    },
    redirect: {
      login: '/homepage',
      logout: '/homepage',
      callback: '/recette',
      home: false,
      accueil: '/recette',
      introduction: '/introduction',
      registerAddress: '/register/address',
      registerCompany: '/register/company'
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: 'https://urstock.herokuapp.com/'
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
}
