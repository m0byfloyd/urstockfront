const menuList = Object.freeze([
  {
    path: '/account',
    displayName: 'Mon compte',
    icon: 'ri-user-line'
  },
  {
    path: '/company/informations',
    displayName: 'Mes informations entreprise',
    icon: 'ri-earth-line'
  },
  {
    path: '/company/shipping-address',
    displayName: 'Adresses de livraison',
    icon: 'ri-map-pin-line'
  },
  // {
  //   path: '/company/payment-methods',
  //   displayName: 'Modes de paiement',
  //   icon: 'ri-bank-card-line'
  // },
  {
    path: '/company/supplier',
    displayName: 'Mes fournisseurs passés',
    icon: 'ri-truck-line'
  },
  {
    path: '/company/orders',
    displayName: 'Mes commandes passées',
    icon: 'ri-shopping-cart-line'
  }
])

export { menuList }
